<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="introduction" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="index"/>

    <revision pkgversion="3.8" date="2013-03-23" status="final"/>
    <revision pkgversion="3.12" date="2014-03-05" status="final"/>
    <revision pkgversion="3.16" date="2015-03-02" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Introdução ao <app>Visualizador de área de trabalho remota</app>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Introdução</title>

  <p>O <app>Visualizador de área de trabalho remota</app> é um aplicativo de visualização de áreas de trabalho remotas para o ambiente GNOME, com o objetivo de acessar outras máquinas usando VNC (Virtual Network Computing) entre outros protocolos. Ele pode ser usado para controlar e interagir com outros computadores ou para simplesmente ver a área de trabalho.</p>

  <p>O <app>Visualizador de área de trabalho remota</app> oferece os seguintes recursos:</p>
  <list>
    <item><p>Conexão a várias máquinas, ao mesmo tempo;</p></item>
    <item><p>Armazenamento de conexões recentes e marcação de favoritas;</p></item>
    <item><p>Navegação na rede local para explorar outras máquinas.</p></item>
  </list>

  <figure>
    <title>Visualizador de área de trabalho em ação</title>
    <media type="image" mime="image/png" src="figures/vinagre-connected-3-16.png">
      <p>Visualizador de área de trabalho remota em execução no GNOME 3, conectado a uma máquina executando GNOME 2</p>
    </media>
  </figure>

</page>
