<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="connect" xml:lang="lv">

  <info>
    <link type="guide" xref="index#connections"/>
    <title type="sort">1</title>

    <revision pkgversion="3.8" date="2013-03-23" status="final"/>
    <revision pkgversion="3.12" date="2014-03-05" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Savienoties ar citu datoru jūsu lokālajā tīklā.</desc>
  </info>

  <title>Izveidot savienojumu</title>

  <p>Jūs varat savienoties ar citiem datoriem jūsu lokālajā tīklā, izmantojot <app>Attālināto darbvirsmas pārlūku</app>.</p>

  <steps>
    <item>
      <p>Izvēlieties <guiseq><gui style="menu">Attālināts</gui><gui style="menuitem">Savienoties</gui></guiseq>.</p>
    </item>
    <item>
      <p>Izvēlieties <gui>Protokols</gui> and the <gui>Host</gui> for the connection.</p>
      <note>
        <p>Daži protokoli atļauj jums redzēt visus pieejamos datorus jūsu lokālajā tīklā spiežot <gui style="button">Meklēt</gui> pogu. Šī poga nebūs redzama, ja nav <app>Avahi</app> atbalsta.</p>
      </note>
    </item>
    <item>
      <p>Izvēlieties kuras opcijas aktivizēt savienojumam. Šīs opcijas mainīsies atkarībā no izmantotā protokola.</p>
    </item>
    <item>
      <p>Spiediet <gui style="button">Savienot</gui> pogu.</p>
      <p>Pie šī soļa attālinātajā darbvirsmā varētu būt nepieciešams apstiprināt savienojumu. Tādā gadījumā skatītājs paliks melns līdz savienojums tiks apstiprināts.</p>
      <note>
        <p>Daži datori var pieprasīt drošu savienojumu: autentifikācijas dialoglodziņš tiks parādīts, prasot jūsu akreditācijas datus. Šo datu veids atkarīgs no attālinātās sistēmas — visticamāk, tas varētu būt parole, kā arī lietotājvārds. Ja jūs atzīmēsiet <gui>Atcerēties šo akreditāciju</gui>, <app>Attālinātais darbvirsmas pārlūks</app> saglabās šo informāciju, izmantojot <app>GNOME Keyring</app>.</p>
      </note>
    </item>
  </steps>

  <p>Ja savienojums jau ir iepriekš izmantots, jūs varam tam piekļūt, izmantojot <guiseq><gui style="menu">Attālināts</gui><gui style="menuitem">Nesenie savienojumi</gui></guiseq>.</p>

  <p>Lai aizvērtu savienojumu, izvēlieties <guiseq><gui style="menu">Attālināts</gui> <gui style="menuitem">Aizvērt</gui></guiseq> vai arī spiediet <gui style="button">Aizvērt</gui> pogu.</p>

</page>
