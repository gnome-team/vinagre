<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="keyboard-shortcuts" xml:lang="fr">

  <info>
    <link type="guide" xref="index#options"/>

    <revision pkgversion="3.8" date="2013-03-23" status="final"/>
    <revision pkgversion="3.12" date="2014-03-05" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Désactiver l'envoi des raccourcis clavier vers la machine distante.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  </info>

  <title>Raccourcis clavier</title>

  <p>Vous pouvez activer ou désactiver les raccourcis clavier en cliquant sur <guiseq><gui style="menu">Affichage</gui><gui style="menuitem">Raccourcis clavier</gui></guiseq>.</p>

  <p>L'activation de cette option vous permet d'utiliser les raccourcis clavier, comme <keyseq><key>Ctrl</key><key>N</key></keyseq>, les accélérateurs de menus et les touches d'accès (telles que <keyseq><key>Alt</key><key>M</key></keyseq>), avec le <app>Visionneur de bureau distant</app>. Cela implique que le <app>Visionneur de bureau distant</app> capture ces combinaisons de touches au lieu de les envoyer à la machine distante. Cette option est désactivée par défaut car la plupart du temps, vous souhaitez interagir avec l'ordinateur auquel vous êtes connecté.</p>

  <note>
    <p>Quand l'option <gui>Raccourcis clavier</gui> est désactivée, <keyseq><key>Ctrl</key><key>Alt</key> <key>Suppr</key></keyseq> est la seule combinaison de touches qui ne sera pas envoyée au bureau distant. Choisissez <guiseq><gui style="menu">Distant</gui><gui style="menuitem">Envoyer Ctrl-Alt-Suppr</gui></guiseq>, ou utilisez le bouton correspondant dans la boîte à outils, pour envoyer cette combinaison de touches.</p>
  </note>

</page>
