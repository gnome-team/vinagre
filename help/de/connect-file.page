<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="connect-file" xml:lang="de">

  <info>
    <link type="guide" xref="index#connections"/>
    <link type="seealso" xref="connect"/>
    <title type="sort">2</title>

    <revision version="0.2" pkgversion="3.8" date="2013-03-23" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@googlemail.com</email>
    </credit>

    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>

    <desc>Verwendung einer Datei für entfernte Verbindungen zum Verbinden mit einem entfernten Rechner.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2008, 2009, 2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2009,2014,2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Mit einer <file>.vnc</file>-Datei verbinden</title>

  <p>Einige Rechner verteilen anstelle von Rechneradresse und Port-Nummer Dateien, die für entfernte Verbindungen erforderlich sind. Diese Dateien enden üblicherweise auf das Suffix <file>.vnc</file> anstelle einer Rechneradresse und Port-Nummer.</p>

  <steps>
    <item>
      <p>Wählen Sie <guiseq><gui style="menu">Entfernt</gui><gui style="menuitem">Öffnen</gui></guiseq>.</p>
    </item>
    <item>
      <p>Wählen Sie die Datei, die Sie öffnen wollen.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui style="button">Öffnen</gui> ein.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Sie können auch eine Datei einer entfernten Verbindung in <app>Dateien</app> öffnen.</p>
  </note>

</page>
