<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="connect" xml:lang="de">

  <info>
    <link type="guide" xref="index#connections"/>
    <title type="sort">1</title>

    <revision pkgversion="3.8" date="2013-03-23" status="final"/>
    <revision pkgversion="3.12" date="2014-03-05" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Eine Verbindung zu einem anderen Rechner in Ihrem lokalen Netzwerk herstellen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2008, 2009, 2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2009,2014,2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Eine Verbindung aufbauen</title>

  <p>Sie können sich zu anderen Rechnern in Ihrem lokalen Netzwerk unter Verwendung von <app>Betrachter für entfernte Bildschirme</app> verbinden.</p>

  <steps>
    <item>
      <p>Wählen Sie <guiseq><gui style="menu">Entfernt</gui><gui style="menuitem">Verbinden</gui></guiseq>.</p>
    </item>
    <item>
      <p>Wählen Sie <gui>Protokoll</gui> und <gui>Rechner</gui> für die Verbindung.</p>
      <note>
        <p>Einige Protokolle ermöglichen es Ihnen, durch Klicken auf den <gui style="button">Suchen</gui>-Knopf, alle verfügbaren Rechner innerhalb Ihres lokalen Netzwerkes anzuzeigen. Falls keine <app>Avahi</app>-Unterstützung vorhanden ist, wird dieser Knopf nicht angezeigt.</p>
      </note>
    </item>
    <item>
      <p>Wählen Sie die Einstellungen, welche aktiv sein sollen, wenn die Verbindung hergestellt wird. Die Einstellungen können sich in Abhängigkeit vom gewählten Protokoll unterscheiden.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <gui style="button">Verbinden</gui>.</p>
      <p>An dieser Stelle kann es erforderlich sein, dass der entfernte Rechner die Verbindung bestätigen muss. In diesem Fall bleibt der Bildschirm schwarz, bis die Verbindung bestätigt wird.</p>
      <note>
        <p>Einige Rechner erfordern eine sichere Verbindung. In diesem Fall wird ein Legitimierungsdialog geöffnet, der Sie um die Eingabe der Legitimationsdaten für die Verbindung bittet. Die Art der Anmeldedaten hängt vom jeweiligen entfernten Rechner ab, meist werden es Benutzername und Passwort sein. Wenn Sie das Feld <gui>Legitimation merken</gui> aktivieren, speichert <app>Betrachter für entfernte Bildschirme</app> diese Information im <app>GNOME-Schlüsselbund</app>.</p>
      </note>
    </item>
  </steps>

  <p>Falls die Verbindung vorher bereits einmal genutzt wurde, erreichen Sie diese auch über <guiseq><gui style="menu">Entfernt</gui><gui style="menuitem">Zuletzt geöffnete Verbindungen</gui></guiseq>.</p>

  <p>Um eine Verbindung zu schließen, wählen Sie <guiseq><gui style="menu">Entfernt</gui><gui style="menuitem">Schließen</gui></guiseq> oder klicken Sie auf den Knopf <gui style="button">Schließen</gui>.</p>

</page>
